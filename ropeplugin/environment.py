import logging

import ropemode.environment

from gi.repository import Gtk, GtkSource

log = logging.getLogger(__name__)


class GEditUtils(ropemode.environment.Environment):
    def __init__(self, window):
        super(GEditUtils, self).__init__()
        self.window = window

    @property
    def doc(self):
        return self.window.get_active_document()

    @property
    def view(self):
        return self.window.get_active_view()

    def ask(self, prompt, default=None, starting=None):
        """
        Collect of

        @param prompt: str ....
        @param default: str if users enters nothing, this is the reply
        @param starting: str default reply
        @return str
        """
        pass

    def ask_values(self, prompt, values, default=None, starting=None):
        pass

    def ask_directory(self, prompt, default=None, starting=None):
        '''
        Let user select a dreictory name

        @param prompt str: prompt used
        @param default str: directory to start in
        @param starting IGNORED
        @return str full path of the selected directory (or None)
        '''
        out_dir = None
        dlg = Gtk.FileChooserDialog(
            title=_(prompt),
            action=Gtk.FileChooserAction.SELECT_FOLDER,
            buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                     Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        # maybe need to initiate save file dialog if uri == None ?
        if default is not None:
            dlg.set_uri(default)
        dlg.set_default_response(Gtk.ResponseType.OK)
        resp = dlg.run()
        if resp == Gtk.ResponseType.OK:
            out_dir = dlg.get_filename()
        dlg.destroy()
        return out_dir

    def ask_completion(self, prompt, values, starting=None):
        pass

    def message(self, message):
        dlg = Gtk.MessageDialog(self.window, 0,
                                Gtk.MessageType.INFO,
                                Gtk.ButtonsType.CLOSE,
                                message)
        dlg.run()
        dlg.destroy()

    def yes_or_no(self, prompt):
        pass

    def y_or_n(self, prompt):
        pass

    def get(self, name, default=None):
        pass

    def get_offset(self):
        #    def get(self, name, default=None):
        #        lispname = 'ropemacs-' + name.replace('_', '-')
        #        if lisp.boundp(lisp[lispname]):
        #            return lisp[lispname].value()
        #        return default
        return self.doc.get_insert.get_line_offset()

    def get_text(self):
        encoding = GtkSource.encoding_get_current().get_charset()

        return self.doc.get_text(self.doc.get_start_iter(),
                                 self.doc.get_end_iter(),
                                 False).decode(encoding)

    def get_region(self):
        #    def get_region(self):
        #        offset1 = self.get_offset()
        #        lisp.exchange_point_and_mark()
        #        offset2 = self.get_offset()
        #        lisp.exchange_point_and_mark()
        #        return min(offset1, offset2), max(offset1, offset2)
        pass

    def filename(self):
        """
        Return full path of the current document as a string.
        """
        return self.doc.get_location().get_path()

    def is_modified(self):
        return self.doc.get_modified()

    def goto_line(self, lineno):
        self.view.get_buffer().goto_line(lineno)
        self.view.scroll_to_cursor()

    def insert_line(self, line, lineno):
        old_insert = self.doc.get_insert()
        cursor = self.doc.get_iter_at_mark(old_insert)

        cursor.set_line(lineno)
        self.doc.insert(cursor, '\n' + line)

        # return to the old position
        self.doc.place_cursor(self.doc.get_iter_at_mark(old_insert))

    def insert(self, text):
        self.view.get_buffer().insert_at_cursor(text)

    def delete(self, start, end):
        #    def delete(self, start, end):
        #        lisp.delete_region(start, end)
        pass

    def filenames(self):
        pass

    def save_files(self, filenames):
        pass

    def reload_files(self, filenames, moves={}):
        pass

    def find_file(self, filename, readonly=False, other=False):
        pass

    def create_progress(self, name):
        pass

    def current_word(self):
        pass

    def push_mark(self):
        pass

    def pop_mark(self):
        pass

    def prefix_value(self, prefix):
        pass

    def show_occurrences(self, locations):
        pass

    def show_doc(self, docs, altview=False):
        pass

    def preview_changes(self, diffs):
        pass

    def local_command(self, name, callback, key=None, prefix=False):
        pass

    def global_command(self, name, callback, key=None, prefix=False):
        pass

    def add_hook(self, name, callback, hook):
        pass

    def _completion_text(self, proposal):
        return proposal.name

    def _completion_data(self, proposal):
        return self._completion_text(proposal)
