# -*- coding:utf-8 -*-
#  This program is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be
#  useful, but WITHOUT ANY WARRANTY; without even the implied
#  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program.  If not, see
#  <http://www.gnu.org/licenses/>.
from __future__ import absolute_import, print_function, unicode_literals

import gettext
import logging
import os.path

import rope.base.project

import ropeplugin.config as config
import ropeplugin.dialogs as dialogs
import ropeplugin.environment as environment

from gi.repository import GObject, Gdk, Gedit, Gio, GtkSource
from rope.base import exceptions, libutils
from rope.contrib import codeassist
from rope.refactor.extract import ExtractVariable

# import ropemode.interface
# to get full ropemode-based system as intended use as:
#
# ropemode.decorators.logger.message = echo
# ropemode.decorators.logger.only_short = True
# _completer = _ValueCompleter()  We probably don't want it

# _init_variables()  # set default values to configurations
# _interface = ropemode.interface.RopeMode(env=_env)
# _interface.init()
# _enable_shortcuts(_env)

# gettext installation
LOCALES_DIR = os.path.join(os.path.dirname(__file__), '.locales')
gettext.install('ropeplugin', LOCALES_DIR, unicode=True)

logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.DEBUG)

log = logging.getLogger('ropeplugin')


def reload_doc(doc):
    'Reloads document from disk'
    # it works, but spits out all sorts of failed assertions.
    # there must be a better way
    doc.load(location=doc.get_location(),
             encoding=doc.get_encoding(),
             column_pos=0, line_pos=0,
             create=False)


def get_selected_text(buffer):
    start, end = buffer.get_selection_bounds()
    return buffer.get_text(start, end)


class RopeProjectHelper(object):
    'A helper class providing rope project management routines.'
    def __init__(self, window):
        self.project = None
        self.window = window
        self._doc = None
        self._env = environment.GEditUtils(self.window)

    @property
    def document(self):
        log.debug('calling document property: %s', self._doc)
        if self._doc is not None:
            return self._doc
        else:
            doc = self.window.get_active_document()
            if doc is not None:
                self._doc = doc
            return doc

    @staticmethod
    def get_file_id(f):
        return f.query_info('id::file',
                            Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS,
                            None).get_attribute_as_string('id::file')

    def file_equals(self, first, second):
        first_id = self.get_file_id(first)
        second_id = self.get_file_id(second)
        return first_id and second_id and first_id == second_id

    def get_or_create_tab_from_file(self, location, line=0):
        '''Returns the tab from uri. If it doesn't exists, opens it.
        If the file doesn't exist, returns nothing.'''
        log.debug('line = %d', line)
        if line > 0:
            line -= 1  # apparently numbering is shifted by 1

        # reimplementation of window.get_tab_from_location which
        # actually works.
        for view in self.window.get_views():
            buf = view.get_buffer()
            if self.file_equals(buf.get_location(), location):
                self._env.goto_line(line)
                tab = view.get_ancestor(Gedit.Tab)
                log.debug('tab = %s', tab)
                return tab

        return self.window.create_tab_from_location(
            location=location, encoding=GtkSource.encoding_get_current(),
            line_pos=line, column_pos=0, create=False, jump_to=True)

    def set_project(self, action, user_data=None):
        'Sets up rope project root folder and creates a rope project.'
        uri = None
        if self.document:
            uri = self.document.get_uri_for_display()

        path = self._env.ask_directory(_(u'Set Project Root Folder…'),
                                       default=uri)
        if path is not None:
            self.project = rope.base.project.Project(path)

    def config_project(self, action, user_data=None):
        '''Opens the current project's config file. If it doesn't exist,
        initiates a 'Set Project Root...' dialog. If config file is already
        open, just switches to its tab.'''
        if self.project is None:
            self.set_project(action, user_data)
        if self.project:  # making sure that project was properly set up
            real_path = self.project.root.real_path
            conf_file = Gio.File.new_for_commandline_arg(
                os.path.join(real_path,
                             '.ropeproject', 'config.py'))
            config_tab = self.get_or_create_tab_from_file(conf_file)
            self.window.set_active_tab(config_tab)


class RefactorHelper(object):
    'Helper class providing refactoring routines'
    def __init__(self, window, project_helper):
        self.window = window
        self.project_helper = project_helper
        self._env = self.project_helper._env

    @property
    def document(self):
        return self.project_helper.document

    @property
    def project(self):
        return self.project_helper.project

    def validate_project(self):
        try:
            self.project.validate()
        except:
            self.project_helper.set_project(action=None)
            self.project.validate()

    def rename(self, action):
        'Renames a variable'
        self.validate_project()

    def reload_modified_documents(self, changes):
        'Reloads all opened docs marked as modified in changes'

    def extract_variable(self, action, user_data=None):
        'Extracts a variable from expression selected in a document'
        self.validate_project()
        doc = self.document
        bounds = doc.get_selection_bounds()
        if bounds is not None:
            start, end = bounds
            if start.get_line_offset() > end.get_line_offset():
                (start, end) = (end, start)  # swap

            resource = libutils.path_to_resource(self.project,
                                                 doc.get_uri_for_display())
            extractor = ExtractVariable(self.project, resource,
                                        start.get_offset(), end.get_offset())
            txt = dialogs.get_python_identifier()
            if txt:
                changes = extractor.get_changes(txt)
                self.apply_changes(changes)

    def apply_changes(self, changes):
        log.info(changes.get_description())
        self.project.do(changes)
        # maybe reloading everything isn't such a good idea...
        for doc in self.window.get_documents():
            reload_doc(doc)

    def ctrl_btn_release_cb(self, widget, event, data=None):
        if event.get_state() & Gdk.ModifierType.CONTROL_MASK:
            log.debug('goto-definition to be activated')
            self.goto_definition()

    # TODO 2Breplaced by ropemode.interface.RopeMode.goto_defintion
    #    @decorators.local_command('a g', shortcut='C-c g')
    #    def goto_definition(self):
    #        definition = self._base_definition_location()
    #        if definition:
    #            self._env.push_mark()
    #            self._goto_location(definition[0], definition[1])
    #        else:
    #            self._env.message('Cannot find the definition!')
    def goto_definition(self):
        self.validate_project()
        doc = self.document
        cursor = doc.get_iter_at_mark(doc.get_insert())
        offset = cursor.get_offset()
        log.debug('offset = %d', offset)
        resource = self.resource
        log.debug('resource = %s', resource)

        # TODO this should be eventually configurable like
        # vimrope's codeassist_maxfixes, default value there is 1
        maxfixes = 1
        try:
            definition = codeassist.get_definition_location(
                self.project, self._env.get_text(), offset, resource, maxfixes)
            log.debug('definition = %s', definition)
        except exceptions.BadIdentifierError:
            definition = None
        if tuple(definition) == (None, None):
            definition = None

        if definition:
            self._goto_location(definition[0], definition[1])
        else:
            self._env.message('Cannot find the definition!')

    def _goto_location(self, resource=None, lineno=0):
        # TODO 2Breplaced by ropemode.interface.RopeMode._goto_location
        log.debug('resource = %s', resource.real_path)
        log.debug('lineno = %d', lineno)
        if resource:
            conf_file = Gio.File.new_for_commandline_arg(resource.real_path)
            config_tab = self.project_helper.get_or_create_tab_from_file(
                conf_file, line=lineno)
            self.window.set_active_tab(config_tab)

    @property
    def resource(self):
        """the current resource

        Returns `None` when file does not exist.
        """
        filename = self.document.get_location().get_path()
        resource = libutils.path_to_resource(self.project, filename, 'file')
        if resource and resource.exists():
            return resource


class RopePluginAppActivatable(GObject.Object, Gedit.AppActivatable):
    __gtype_name__ = 'RopePlugin'
    app = GObject.property(type=Gedit.App)

    def __init__(self):
        GObject.Object.__init__(self)
        logging.debug('self.app = %s', self.app)

    def do_activate(self):
        # CONFIG_PLUGIN_SHORTCUT = None
        self.app.set_accels_for_action("win.set_project",
                                       [config.SET_PROJECT_SHORTCUT])
        self.app.set_accels_for_action("win.config_project",
                                       [config.CONFIG_PROJECT_SHORTCUT])
        self.app.set_accels_for_action("win.extract_variable",
                                       [config.EXTRACT_VARIABLE_SHORTCUT])
        # msgid "Configure Plugin"

        self.menu_ext = self.extend_menu("tools-section")
        item = Gio.MenuItem.new(_("Set Project Root Folder..."),
                                "win.set_project")
        self.menu_ext.prepend_menu_item(item)
        item = Gio.MenuItem.new(_("Configure Project"),
                                "win.config_project")
        self.menu_ext.prepend_menu_item(item)
        item = Gio.MenuItem.new(_("Extract Variable"),
                                "win.extract_variable")
        self.menu_ext.prepend_menu_item(item)

    def do_deactivate(self):
        self.app.set_accels_for_action("win.set_project", [])
        self.app.set_accels_for_action("win.config_project", [])
        self.app.set_accels_for_action("win.extract_variable", [])
        self.menu_ext = None

    def do_update_state(self):
        pass


class RopePluginWindowActivatable(GObject.Object, Gedit.WindowActivatable):
    'Main plugin class providing integration with Gedit UI'
    __gtype_name__ = 'RopePluginWindowActivatable'

    window = GObject.property(type=Gedit.Window)

    def __init__(self):
        GObject.Object.__init__(self)
        self.settings = Gio.Settings.new("org.gnome.gedit.preferences.editor")

    def do_activate(self):
        log.debug('RopePluginWindowActivatable activate!')

        self.project_helper = RopeProjectHelper(self.window)
        self.refactor_helper = RefactorHelper(self.window,
                                              self.project_helper)
        # set_project initiaze
        action = Gio.SimpleAction(name="set_project")
        action.connect('activate', self.project_helper.set_project)
        self.window.add_action(action)

        action = Gio.SimpleAction(name="config_project")
        action.connect('activate', self.project_helper.config_project)
        self.window.add_action(action)

        action = Gio.SimpleAction(name="extract_variable")
        action.connect('activate', self.refactor_helper.extract_variable)
        self.window.add_action(action)

        # Activate document
        self.active_tab_state_call_id = self.window.connect(
            'active-tab-state-changed', self.__active_tab_state_cb)

    def __active_tab_state_cb(self, window):
        log.debug('__active_tab_state_cb callback called!')
        if self.project_helper.document is not None:
            log.debug('COnnecting click event handler!')
            self.active_view_click_id = \
                self.window.get_active_view().connect(
                    "button-release-event",
                    self.refactor_helper.ctrl_btn_release_cb)

            window.disconnect(self.active_tab_state_call_id)

    def config_plugin(self, action):
        config_file = Gio.File.new_for_path(
            os.path.join(os.path.dirname(__file__), 'config.py'))
        config_tab = self.project_helper.get_or_create_tab_from_file(
            config_file)
        self.window.set_active_tab(config_tab)
